# Invilla Frontend Test

Create a list of Star Wars characters and detail all about then and your starships.

## Getting Started

```shell
git clone meu-repo
yarn
yarn start
```

## Start unit tests

```shell
yarn
yarn test
```
