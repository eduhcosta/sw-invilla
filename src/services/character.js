import axios from "./../core/axios.config";

import { errorHandler } from "./../utils";

const getAll = (page = 1) =>
  axios.get(`/people/?page=${page}`).catch(error => errorHandler(error));

const getById = id =>
  axios.get(`/people/${id}`).catch(error => errorHandler(error));

export { getAll, getById };
