import axios from "./../core/axios.config";

import { errorHandler } from "./../utils";

const getById = id =>
  axios.get(`/starships/${id}`).catch(error => errorHandler(error));

export { getById };
