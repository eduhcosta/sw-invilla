import React from "react";
import { BrowserRouter } from "react-router-dom";
import { CssBaseline } from "@material-ui/core";

import { Header } from "./../components";
import Routes from "./Routes";

const App = () => (
  <BrowserRouter>
    <CssBaseline />
    <Header />
    <Routes />
  </BrowserRouter>
);

export default App;
