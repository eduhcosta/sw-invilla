import React from 'react';
import { Switch, Route } from 'react-router-dom';

// Components
import Home from './../screens/Home';
import Character from './../screens/Character';

const Routes = () => (
  <Switch>
    <Route exact path="/" component={Home} />
    <Route exact path="/character/:id" component={Character} />
  </Switch>
);

export default Routes;
