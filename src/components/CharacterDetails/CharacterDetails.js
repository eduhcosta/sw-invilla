import React from "react";
import PropTypes from "prop-types";
import { Typography, Grid, Divider } from "@material-ui/core";
import ShowData from "./../ShowData";
import StarshipsList from "./../StarshipsList";
import useStyles from "./CharacterDetails.style";
import { upperFirst } from "../../utils";

const CharacterDetails = ({ data, id, shipsIds }) => {
  const classes = useStyles();
  return (
    <div className={classes.container}>
      <Grid container spacing={2} className={classes.gridClass}>
        <Grid item lg={3}>
          <img
            src={`https://starwars-visualguide.com/assets/img/characters/${id}.jpg`}
            alt={data?.name}
            title={data?.name}
            className={classes.image}
            data-testid="character-image"
          />
        </Grid>
        <Grid item lg={9}>
          <div className={classes.descriptionSection}>
            <Typography
              component="h1"
              className={classes.title}
              data-testid="character-name"
            >
              {data?.name}
            </Typography>
            <Divider className={classes.divider} />
            <Grid container spacing={2} className={classes.secondaryGrid}>
              <Grid item lg={2}>
                <ShowData
                  icon="star"
                  text={data?.birth_year}
                  data-testid="character-birth-year"
                />
              </Grid>
              <Grid item lg={2}>
                <ShowData
                  icon="height"
                  text={data?.height + "cm"}
                  data-testid="character-height"
                />
              </Grid>
              <Grid item lg={2}>
                <ShowData
                  icon="fastfood"
                  text={data?.mass + "kg"}
                  data-testid="character-mass"
                />
              </Grid>
              <Grid item lg={3}>
                <ShowData
                  icon="face"
                  text={upperFirst(data?.hair_color)}
                  data-testid="character-hair"
                />
              </Grid>
              <Grid item lg={2}>
                <ShowData
                  icon="color_lens"
                  text={upperFirst(data?.skin_color)}
                  data-testid="character-skin"
                />
              </Grid>
              <Grid item lg={2}>
                <ShowData
                  icon="visibility"
                  text={upperFirst(data?.eye_color)}
                  data-testid="character-eye"
                />
              </Grid>
              <Grid item lg={2}>
                <ShowData
                  icon="wc"
                  text={upperFirst(data?.gender)}
                  data-testid="character-gender"
                />
              </Grid>
            </Grid>
            <Typography
              component="h3"
              className={classes.secondaryTitle}
              data-testid="character-starship"
            >
              Starships of {data?.name}
            </Typography>
            <Divider className={classes.divider} />
            {shipsIds && <StarshipsList ids={shipsIds} />}
          </div>
        </Grid>
      </Grid>
    </div>
  );
};

CharacterDetails.propTypes = {
  data: PropTypes.object.isRequired,
  id: PropTypes.string.isRequired,
  shipsIds: PropTypes.array
};

export default CharacterDetails;
