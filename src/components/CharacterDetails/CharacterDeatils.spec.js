import React from "react";
import { render } from "@testing-library/react";
import { MemoryRouter } from "react-router-dom";

import CharacterDetails from ".";

const dataMock = {
  name: "Good Test",
  birth_year: "TESTBIRTH",
  height: 185,
  mass: 77,
  hair_color: "brown",
  eye_color: "black",
  gender: "no one"
};

const renderComponent = (overrideProps = {}) => {
  const props = {
    data: dataMock,
    id: "2",
    shipsIds: [],
    ...overrideProps
  };
  return render(
    <MemoryRouter initialEntries={["/"]}>
      <CharacterDetails {...props} />
    </MemoryRouter>
  );
};

describe("CharacterDetails Component", () => {
  it("Should be rendered a component", () => {
    const { container } = renderComponent();
    expect(container.firstChild).toBeDefined();
  });

  it("Should be rendered a character-image", () => {
    const { queryByTestId } = renderComponent();
    const element = queryByTestId("character-image");
    expect(element).toBeDefined();
  });

  it("Should be defined correctly the character-image", () => {
    const { queryByTestId } = renderComponent({ id: "3" });
    const element = queryByTestId("character-image");
    const src = element.src;
    expect(src.includes("3.jpg")).toBeTruthy();
  });

  it("Should be rendered a character-birth-year", () => {
    const { queryByTestId } = renderComponent();
    const element = queryByTestId("character-birth-year");
    expect(element).toBeDefined();
  });

  it("Should be defined correctly the character-birth-year", () => {
    const { queryByTestId } = renderComponent({
      data: { ...dataMock, birth_year: "TESTBIRTH" }
    });
    const element = queryByTestId("character-birth-year");
    const text = element.childNodes[1].textContent;
    expect(text).toEqual("TESTBIRTH");
  });

  it("Should be rendered a character-height", () => {
    const { queryByTestId } = renderComponent();
    const element = queryByTestId("character-height");
    expect(element).toBeDefined();
  });

  it("Should be defined correctly the character-height", () => {
    const { queryByTestId } = renderComponent({
      data: { ...dataMock, height: 154 }
    });
    const element = queryByTestId("character-height");
    const text = element.childNodes[1].textContent;
    expect(text).toEqual("154cm");
  });

  it("Should be rendered a character-mass", () => {
    const { queryByTestId } = renderComponent();
    const element = queryByTestId("character-mass");
    expect(element).toBeDefined();
  });

  it("Should be defined correctly the character-mass", () => {
    const { queryByTestId } = renderComponent({
      data: { ...dataMock, mass: 86 }
    });
    const element = queryByTestId("character-mass");
    const text = element.childNodes[1].textContent;
    expect(text).toEqual("86kg");
  });

  it("Should be rendered a character-hair", () => {
    const { queryByTestId } = renderComponent();
    const element = queryByTestId("character-hair");
    expect(element).toBeDefined();
  });

  it("Should be defined correctly the character-hair", () => {
    const { queryByTestId } = renderComponent({
      data: { ...dataMock, hair_color: "white" }
    });
    const element = queryByTestId("character-hair");
    const text = element.childNodes[1].textContent;
    expect(text).toEqual("White");
  });

  it("Should be rendered a character-skin", () => {
    const { queryByTestId } = renderComponent();
    const element = queryByTestId("character-skin");
    expect(element).toBeDefined();
  });

  it("Should be defined correctly the character-skin", () => {
    const { queryByTestId } = renderComponent({
      data: { ...dataMock, skin_color: "brown" }
    });
    const element = queryByTestId("character-skin");
    const text = element.childNodes[1].textContent;
    expect(text).toEqual("Brown");
  });

  it("Should be rendered a character-eye", () => {
    const { queryByTestId } = renderComponent();
    const element = queryByTestId("character-eye");
    expect(element).toBeDefined();
  });

  it("Should be defined correctly the character-eye", () => {
    const { queryByTestId } = renderComponent({
      data: { ...dataMock, eye_color: "brown" }
    });
    const element = queryByTestId("character-eye");
    const text = element.childNodes[1].textContent;
    expect(text).toEqual("Brown");
  });

  it("Should be rendered a character-gender", () => {
    const { queryByTestId } = renderComponent();
    const element = queryByTestId("character-gender");
    expect(element).toBeDefined();
  });

  it("Should be defined correctly the character-gender", () => {
    const { queryByTestId } = renderComponent({
      data: { ...dataMock, gender: "male" }
    });
    const element = queryByTestId("character-gender");
    const text = element.childNodes[1].textContent;
    expect(text).toEqual("Male");
  });

  it("Should be rendered a character-starship", () => {
    const { queryByTestId } = renderComponent();
    const element = queryByTestId("character-starship");
    expect(element).toBeDefined();
  });

  it("Should be defined correctly the character-starship", () => {
    const { queryByTestId } = renderComponent({
      data: { ...dataMock, name: "Good Test" }
    });
    const element = queryByTestId("character-starship");
    const text = element.textContent;
    expect(text).toEqual("Starships of Good Test");
  });
});
