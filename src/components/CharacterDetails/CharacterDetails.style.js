import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles({
  container: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    flex: 1
  },
  gridClass: {
    maxWidth: "80vw",
    marginBottom: 0
  },
  secondaryGrid: {
    marginBottom: "2em"
  },
  image: {
    width: "100%",
    margin: "1em",
    border: "2px solid white"
  },
  descriptionSection: {
    color: "white",
    margin: "1em",
    paddingLeft: "1em"
  },
  title: {
    fontSize: "3em",
    fontWeight: "semibold"
  },
  secondaryTitle: {
    fontSize: "2em"
  },
  divider: {
    backgroundColor: "white",
    width: "100%",
    marginBottom: "1em"
  }
});

export default useStyles;
