import React from "react";
import PropTypes from "prop-types";

import Starships from "./../Starships";
import { Typography } from "@material-ui/core";

const StarshipsList = ({ ids }) => {
  if (ids.length === 0) {
    return <Typography data-testid="empty-message">No one</Typography>;
  }
  return ids.map(item => <Starships key={item} id={item} />);
};

StarshipsList.propTypes = {
  ids: PropTypes.array.isRequired
};

export default StarshipsList;
