import React from "react";
import { render } from "@testing-library/react";

import StarshipsList from ".";

const renderComponent = (overrideProps = {}) => {
  const props = {
    ids: [],
    ...overrideProps
  };
  return render(<StarshipsList {...props} />);
};

describe("StarshipsList Component", () => {
  it("Should be render a component", () => {
    const { container } = renderComponent();
    expect(container.firstChild).toBeDefined();
  });

  it("Should be rendered a empty-message", () => {
    const { getByTestId } = renderComponent();
    const element = getByTestId("empty-message");
    expect(element).toBeTruthy();
  });

  it("Should be rendered two items", async () => {
    const { container } = renderComponent({ ids: [22, 33] });
    expect(container.childNodes).toHaveLength(2);
  });
});
