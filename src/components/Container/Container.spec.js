import React from "react";
import { render } from "@testing-library/react";

import Container from "./";

const renderComponent = (overrideProps = {}) => {
  const props = {
    children: <div />,
    ...overrideProps
  };
  return render(<Container {...props} />);
};

describe("Container Component", () => {
  it("Should be render a component", () => {
    const { container } = renderComponent();
    expect(container.firstChild).toBeDefined();
  });

  it("Should be define a container prop", () => {
    const { getByTestId } = renderComponent({ "data-testid": "good-test" });
    const element = getByTestId("good-test");
    expect(element).toBeTruthy();
  });
});
