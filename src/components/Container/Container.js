import React from "react";
import PropTypes from "prop-types";

import { Paper } from "@material-ui/core";
import useStyles from "./Container.style";

const Container = ({ children, ...props }) => {
  const classes = useStyles();
  return (
    <Paper className={classes.paper} {...props}>
      {children}
    </Paper>
  );
};

Container.propTypes = {
  children: PropTypes.node.isRequired
};

export default Container;
