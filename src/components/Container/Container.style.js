import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(() => ({
  paper: {
    border: "none",
    boxShadow: "none",
    borderRadius: 0,
    background:
      "url(http://www.script-tutorials.com/demos/360/images/stars.png) top center #000",
    minHeight: "calc(100vh - 64px)"
  }
}));

export default useStyles;
