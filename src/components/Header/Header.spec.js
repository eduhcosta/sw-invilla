import React from "react";
import { render } from "@testing-library/react";
import { MemoryRouter } from "react-router-dom";

import Header from ".";

const renderComponent = (overrideProps = {}) => {
  const props = { ...overrideProps };
  return render(
    <MemoryRouter initialEntries={["/"]}>
      <Header {...props} />
    </MemoryRouter>
  );
};

describe("Header Component", () => {
  it("Should be render a component", () => {
    const { container } = renderComponent();
    expect(container.firstChild).toBeDefined();
  });

  it("Should be rendered a app-header", () => {
    const { getByTestId } = renderComponent();
    const element = getByTestId("app-header");
    expect(element).toBeTruthy();
  });

  it("Should be rendered a app-home", () => {
    const { getByTestId } = renderComponent();
    const element = getByTestId("app-home");
    expect(element).toBeTruthy();
  });
});
