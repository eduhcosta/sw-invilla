import React from "react";

import { useHistory } from "react-router-dom";
import useStyles from "./Header.style";

const Header = () => {
  const classes = useStyles();
  const history = useHistory();

  return (
    <div className={classes.container} data-testid="app-header">
      <div className={classes.column}>
        <img
          data-testid="app-home"
          alt="star-wars logo"
          src="https://3dwarehouse.sketchup.com/warehouse/v1.0/publiccontent/b6d0cc66-d8b7-42c0-985d-48c32d3d8de6"
          className={classes.img}
          onClick={() => history.push("/")}
        />
      </div>
    </div>
  );
};

Header.propTypes = {};

export default Header;
