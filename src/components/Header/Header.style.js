import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(theme => ({
  container: {
    height: 64,
    display: "flex",
    justifyContent: "center",
    background:
      "url(http://www.script-tutorials.com/demos/360/images/stars.png) top center #000"
  },
  img: {
    height: 64,
    "&:hover": { cursor: "pointer" }
  },
  column: {
    width: "80vw"
  }
}));

export default useStyles;
