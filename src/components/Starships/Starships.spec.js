import React from "react";
import { render } from "@testing-library/react";
import { MemoryRouter } from "react-router-dom";

import Starships from "./";

const renderComponent = (overrideProps = {}) => {
  const props = {
    id: 2,
    ...overrideProps
  };
  return render(
    <MemoryRouter initialEntries={["/"]}>
      <Starships {...props} />
    </MemoryRouter>
  );
};

describe("Starships Component", () => {
  it("Should be render a component", () => {
    const { container } = renderComponent();
    expect(container.firstChild).toBeDefined();
  });

  it("Should be render a loader", () => {
    const { getByTestId } = renderComponent();
    const element = getByTestId("starship-loader");
    expect(element).toBeTruthy();
  });
});
