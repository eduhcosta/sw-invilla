import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import { Typography, Grid } from "@material-ui/core";

import { getById } from "./../../services/starships";
import { upperFirst } from "./../../utils";
import useStyles from "./Strships.style";

const Starships = ({ id }) => {
  const classes = useStyles();
  const [data, setData] = useState(null);

  const removeNoUserKeys = res => {
    delete res["pilots"];
    delete res["films"];
    delete res["created"];
    delete res["edited"];
    delete res["url"];
    return res;
  };

  const normalizeObject = props => {
    let newResponse = Object.entries(props);
    newResponse = newResponse.map(item => {
      let newKey = upperFirst(item[0]);
      newKey = newKey.replace(/_/g, " ");
      return [newKey, item[1]];
    });
    return newResponse;
  };

  const loadData = () => {
    getById(id).then(({ data: response }) => {
      let newResponse = removeNoUserKeys(response);
      setData(newResponse);
    });
  };

  useEffect(loadData, []);

  const composePropList = (startIndex, size) => {
    const propsData = normalizeObject(data);
    const renderedItems = propsData.map((prop, index) => {
      if (index < startIndex || index > startIndex + size) return null;
      return (
        <li className={classes.itemList} data-testid="starship-prop">
          <Typography className={classes.label}>{prop[0]}:</Typography>
          <Typography className={classes.value}>{prop[1]}</Typography>
        </li>
      );
    });

    return <ul className={classes.list}>{renderedItems}</ul>;
  };

  return data ? (
    <Grid
      container
      spacing={2}
      className={classes.root}
      data-testid="starship-item"
    >
      <Grid item lg={6}>
        <div className={classes.containerImage}>
          <Typography className={classes.title} data-testid="starship-name">
            {data.name}
          </Typography>
          <img
            data-testid="starship-image"
            className={classes.image}
            alt={data.name}
            src={`https://starwars-visualguide.com/assets/img/starships/${id}.jpg`}
          />
        </div>
      </Grid>
      <Grid item lg={6}>
        {composePropList(1, 12)}
      </Grid>
    </Grid>
  ) : (
    <Typography data-testid="starship-loader">Loading...</Typography>
  );
};

Starships.propTypes = {
  id: PropTypes.number.isRequired
};

export default Starships;
