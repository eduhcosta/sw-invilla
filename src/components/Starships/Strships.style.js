import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles({
  root: {
    backgroundColor: "white",
    color: "#333",
    marginBottom: "1em",
    marginTop: "1em"
  },
  title: {
    fontSize: 18,
    position: "absolute",
    padding: ".5em",
    bottom: ".3em",
    color: "white",
    width: "100%",
    backgroundColor: "rgba(0, 0, 0, 0.4)"
  },
  containerImage: {
    display: "flex",
    alignItems: "center",
    position: "relative"
  },
  image: {
    width: "100%"
  },
  list: {
    padding: 0,
    margin: 0
  },
  itemList: {
    display: "flex"
  },
  label: {
    fontWeight: "bold",
    marginRight: ".6em",
    fontSize: 14
  },
  value: {
    fontSize: 14
  }
});

export default useStyles;
