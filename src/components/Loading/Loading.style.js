import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(() => ({
  container: {
    with: "100%",
    height: "100%",
    minHeight: "calc(100vh - 64px)",
    minWidth: "98vw",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "column",
    color: "white",
    background:
      "url(http://www.script-tutorials.com/demos/360/images/stars.png) top center #000"
  },
  circle: {
    width: "100px !important",
    height: "100px !important",
    marginBottom: 30
  }
}));

export default useStyles;
