import React from "react";
import { render } from "@testing-library/react";
import { MemoryRouter } from "react-router-dom";

import Loading from ".";

const renderComponent = (overrideProps = {}) => {
  const props = { ...overrideProps };
  return render(
    <MemoryRouter initialEntries={["/"]}>
      <Loading {...props} />
    </MemoryRouter>
  );
};

describe("Loading Component", () => {
  it("Should be render a component", () => {
    const { container } = renderComponent();
    expect(container.firstChild).toBeDefined();
  });

  it("Should be rendered a loading-container", () => {
    const { getByTestId } = renderComponent();
    const element = getByTestId("loading-container");
    expect(element).toBeTruthy();
  });

  it("Should be rendered a loading-progress", () => {
    const { getByTestId } = renderComponent();
    const element = getByTestId("loading-progress");
    expect(element).toBeTruthy();
  });

  it("Should be rendered a loading-text", () => {
    const { getByTestId } = renderComponent();
    const element = getByTestId("loading-text");
    expect(element).toBeTruthy();
  });

  it("Should be rendered a loading-text with `Loading...` content", () => {
    const { getByTestId } = renderComponent();
    const element = getByTestId("loading-text");
    expect(element.textContent).toEqual("Loading...");
  });
});
