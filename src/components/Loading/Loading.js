import React from "react";

import CircularProgress from "@material-ui/core/CircularProgress";
import useStyles from "./Loading.style";
import { Typography } from "@material-ui/core";
import Container from "../Container";

const Loading = () => {
  const classes = useStyles();
  return (
    <Container className={classes.container} data-testid="loading-container">
      <CircularProgress
        className={classes.circle}
        data-testid="loading-progress"
      />
      <Typography data-testid="loading-text">Loading...</Typography>
    </Container>
  );
};

export default Loading;
