import React from "react";
import PropTypes from "prop-types";

import { Grid } from "@material-ui/core";
import Card from "./../Card";

const CardList = ({ charList }) => {
  const cardsMap = charList.map((item, index) => {
    let parsedId = item?.url.replace("https://swapi.co/api/people/", "");
    parsedId = parsedId.replace("/", "");
    parsedId = parseInt(parsedId);
    return (
      <Card
        key={index}
        charName={item?.name}
        born={item?.birth_year}
        gender={item?.gender}
        id={parsedId}
      />
    );
  });

  return (
    <Grid
      container
      spacing={2}
      style={{ maxWidth: "80vw" }}
      data-testid="card-list-container"
    >
      {cardsMap}
    </Grid>
  );
};

CardList.propTypes = {
  charList: PropTypes.array.isRequired
};

export default CardList;
