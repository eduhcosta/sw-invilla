import React from "react";
import { render } from "@testing-library/react";
import { MemoryRouter } from "react-router-dom";

import CardList from ".";

const renderComponent = (overrideProps = {}) => {
  const props = {
    charList: [],
    ...overrideProps
  };
  return render(
    <MemoryRouter initialEntries={["/"]}>
      <CardList {...props} />
    </MemoryRouter>
  );
};

describe("CardList Component", () => {
  it("Should be render a component", () => {
    const { container } = renderComponent();
    expect(container.firstChild).toBeDefined();
  });

  it("Should be no cards", () => {
    const { queryByTestId } = renderComponent();
    const element = queryByTestId("card-list-container");
    expect(element.firstChild).toBeNull();
  });

  it("Should be one card", () => {
    const { queryByTestId } = renderComponent({
      charList: [
        {
          name: "Good",
          birth_year: "154BBY",
          gender: "no one",
          url: "2"
        }
      ]
    });
    const element = queryByTestId("card-list-container");
    expect(element.firstChild).toBeDefined();
  });

  it("Should be cards equal of length od charList array", () => {
    const charListArray = [
      {
        name: "Good",
        birth_year: "154BBY",
        gender: "no one",
        url: "2"
      },
      {
        name: "Good",
        birth_year: "154BBY",
        gender: "no one",
        url: "2"
      },
      {
        name: "Good",
        birth_year: "154BBY",
        gender: "no one",
        url: "2"
      }
    ];
    const { queryByTestId } = renderComponent({ charList: charListArray });
    const element = queryByTestId("card-list-container");
    expect(element.childNodes.length).toEqual(charListArray.length);
  });
});
