import React from "react";
import PropTypes from "prop-types";

import { Typography } from "@material-ui/core";
import Icon from "@material-ui/core/Icon";
import useStyles from "./ShowData.style";

const ShowData = ({ icon, text, ...rest }) => {
  const classes = useStyles();
  return (
    <div className={classes.container} {...rest}>
      <Icon className={classes.icon} data-testid="show-data-icon">
        {icon}
      </Icon>
      <Typography data-testid="show-data-text">{text}</Typography>
    </div>
  );
};

ShowData.propTypes = {
  icon: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired
};

export default ShowData;
