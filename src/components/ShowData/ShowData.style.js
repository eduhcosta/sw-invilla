import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles({
  container: {
    display: "flex",
    alignItems: "center",
    flex: 1
  },
  icon: {
    marginRight: ".5em"
  }
});

export default useStyles;
