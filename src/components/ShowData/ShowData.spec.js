import React from "react";
import { render } from "@testing-library/react";
import { MemoryRouter } from "react-router-dom";

import Loading from ".";

const renderComponent = (overrideProps = {}) => {
  const props = {
    "data-testid": "show-data-test",
    icon: "start",
    text: "Good Test",
    ...overrideProps
  };
  return render(
    <MemoryRouter initialEntries={["/"]}>
      <Loading {...props} />
    </MemoryRouter>
  );
};

describe("Loading Component", () => {
  it("Should be render a component", () => {
    const { container } = renderComponent();
    expect(container.firstChild).toBeDefined();
  });

  it("Should be rendered a show-data-test", () => {
    const { getByTestId } = renderComponent();
    const element = getByTestId("show-data-test");
    expect(element).toBeTruthy();
  });

  it("Should be rendered a show-data-icon", () => {
    const { getByTestId } = renderComponent();
    const element = getByTestId("show-data-icon");
    expect(element).toBeTruthy();
  });

  it("Should be set a Icon", () => {
    const { getByTestId } = renderComponent({ icon: "height" });
    const element = getByTestId("show-data-icon");
    expect(element.textContent).toEqual("height");
  });

  it("Should be rendered a show-data-text", () => {
    const { getByTestId } = renderComponent();
    const element = getByTestId("show-data-text");
    expect(element).toBeTruthy();
  });

  it("Should be set a text of show-data-text", () => {
    const { getByTestId } = renderComponent({ text: "Good test" });
    const element = getByTestId("show-data-text");
    expect(element.textContent).toEqual("Good test");
  });
});
