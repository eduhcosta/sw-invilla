import React from "react";
import PropTypes from "prop-types";

import { useHistory } from "react-router-dom";
import {
  Grid,
  Card,
  CardActions,
  CardContent,
  CardMedia,
  Button,
  Typography
} from "@material-ui/core";
import useStyles from "./Card.style";

const CardComponent = ({ charName, born, gender, id }) => {
  const classes = useStyles();
  let history = useHistory();

  const goToCharacterPage = () => {
    history.push(`/character/${id}`);
  };

  return (
    <Grid item lg={3}>
      <Card
        className={classes.root}
        variant="outlined"
        data-testid="character-card"
      >
        <CardMedia
          className={classes.media}
          image={`https://starwars-visualguide.com/assets/img/characters/${id}.jpg`}
          title={charName}
          data-testid="character-image"
        />
        <CardContent>
          <Typography variant="h5" component="h2" data-testid="character-name">
            {charName}
          </Typography>
          <Typography
            className={classes.pos}
            color="textSecondary"
            data-testid="character-born-gender"
          >
            {born} - {gender}
          </Typography>
        </CardContent>
        <CardActions>
          <Button
            size="small"
            color="primary"
            onClick={goToCharacterPage}
            data-testid="learn-more-button"
          >
            Learn More
          </Button>
        </CardActions>
      </Card>
    </Grid>
  );
};

CardComponent.propTypes = {
  charName: PropTypes.string.isRequired,
  born: PropTypes.string.isRequired,
  gender: PropTypes.string.isRequired,
  id: PropTypes.number.isRequired
};

export default CardComponent;
