import React from "react";
import { render } from "@testing-library/react";
import { MemoryRouter } from "react-router-dom";

import Card from "./";

const renderComponent = (overrideProps = {}) => {
  const props = {
    charName: "Good Card",
    born: "BB123Y",
    gender: "no one",
    id: 2,
    ...overrideProps
  };
  return render(
    <MemoryRouter initialEntries={["/"]}>
      <Card {...props} />
    </MemoryRouter>
  );
};

describe("Card Component", () => {
  it("Should be render a component", () => {
    const { container } = renderComponent();
    expect(container.firstChild).toBeDefined();
  });

  it("Should be render a character-card", () => {
    const { getByTestId } = renderComponent();
    const element = getByTestId("character-card");
    expect(element).toBeTruthy();
  });

  it("Should be render a character-image", () => {
    const { getByTestId } = renderComponent();
    const element = getByTestId("character-image");
    expect(element).toBeTruthy();
  });

  it("Should be render a the correct image", () => {
    const { getByTestId } = renderComponent({ id: 3 });
    const element = getByTestId("character-image");
    const backgroundStyle = element.style.backgroundImage;
    expect(backgroundStyle.includes("3.jpg")).toBeTruthy();
  });

  it("Should be render a character-name", () => {
    const { getByTestId } = renderComponent();
    const element = getByTestId("character-name");
    expect(element).toBeTruthy();
  });

  it("Should be render a correct Name", () => {
    const { getByTestId } = renderComponent({ charName: "testName" });
    const element = getByTestId("character-name");
    expect(element.firstChild.nodeValue).toEqual("testName");
  });

  it("Should be render a character-born-gender", () => {
    const { getByTestId } = renderComponent();
    const element = getByTestId("character-born-gender");
    expect(element).toBeTruthy();
  });

  it("Should be render a correct format of born and gender", () => {
    const { getByTestId } = renderComponent({
      born: "a",
      gender: "b"
    });
    const element = getByTestId("character-born-gender");
    expect(element.firstChild.nodeValue).toEqual("a");
    expect(element.childNodes[1].nodeValue).toEqual(" - ");
    expect(element.childNodes[2].nodeValue).toEqual("b");
  });

  it("Should be render a learn-more-button", () => {
    const { getByTestId } = renderComponent();
    const element = getByTestId("learn-more-button");
    expect(element).toBeTruthy();
  });
});
