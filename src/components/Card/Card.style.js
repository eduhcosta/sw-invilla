import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles({
  root: {
    width: 275,
    margin: 10,
    borderRadius: 0,
    border: "none"
  },
  media: {
    height: 300
  },
  bullet: {
    display: "inline-block",
    margin: "0 2px",
    transform: "scale(0.8)"
  },
  title: {
    fontSize: 14
  }
});

export default useStyles;
