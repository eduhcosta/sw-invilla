export default (ArrayIds, prefix) => {
  if (!ArrayIds || !prefix) {
    return null;
  }

  const ids = ArrayIds.map(item => {
    let parsedItem = item.replace(prefix, "");
    parsedItem = parsedItem.replace("/", "");
    return parseInt(parsedItem);
  });

  return ids;
};
