import React from "react";
import { render } from "@testing-library/react";

import Home from "./";

const renderComponent = (overrideProps = {}) => {
  const props = {
    ...overrideProps
  };
  return render(<Home {...props} />);
};

describe("Home Component", () => {
  it("Should be render a component", () => {
    const { container } = renderComponent();
    expect(container.firstChild).toBeDefined();
  });
});
