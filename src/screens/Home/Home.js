import React, { useEffect, useState } from "react";

// Services
import { getAll } from "./../../services/character";
// Components
import { Pagination } from "@material-ui/lab";
import { Container, CardList, Loading } from "./../../components";
// Styles
import useStyles from "./Home.style";

const Home = props => {
  const [actualPage, setPage] = useState(1);
  const [totalPages, setTotal] = useState(0);
  const [isLoading, setLoading] = useState(true);
  const [charList, setCharList] = useState([]);
  const classes = useStyles();

  const loadMore = () => {
    getAll(actualPage).then(({ data }) => {
      setTotal(data?.count);
      setCharList(data?.results);
      setLoading(false);
    });
  };

  const onChangePage = (_, page) => {
    setLoading(true);
    setPage(page);
  };

  useEffect(loadMore, [actualPage]);

  return (
    <Container>
      <div className={classes.cards}>
        {isLoading ? <Loading /> : <CardList charList={charList} />}
      </div>
      <div className={classes.pagination}>
        <Pagination
          color="primary"
          shape="rounded"
          count={totalPages}
          page={actualPage}
          onChange={onChangePage}
        />
      </div>
    </Container>
  );
};

export default Home;
