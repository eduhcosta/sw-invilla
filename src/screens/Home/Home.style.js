import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(() => ({
  cards: {
    height: "calc(100vh - 106px)",
    display: "flex",
    overflowY: "scroll",
    justifyContent: "center"
  },
  pagination: {
    padding: 5,
    backgroundColor: "white"
  }
}));

export default useStyles;
