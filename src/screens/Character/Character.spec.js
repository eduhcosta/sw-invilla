import React from "react";
import { render } from "@testing-library/react";
import { MemoryRouter } from "react-router-dom";

import Character from "./";

const renderComponent = (overrideProps = {}) => {
  const props = {
    ...overrideProps
  };
  return render(
    <MemoryRouter>
      <Character {...props} />
    </MemoryRouter>
  );
};

describe("Character Component", () => {
  it("Should be render a component", () => {
    const { container } = renderComponent();
    expect(container.firstChild).toBeDefined();
  });
});
