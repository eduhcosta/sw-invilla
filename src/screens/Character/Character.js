import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { getById } from "./../../services/character";
import { Container, Loading, CharacterDetails } from "./../../components";
import { onlyIds } from "./../../utils";

const Character = () => {
  const [charData, setCharData] = useState(null);
  const [starships, setShips] = useState(null);

  let { id } = useParams();

  const loadCharData = () => {
    getById(id).then(({ data }) => {
      setCharData(data);
      setShips(onlyIds(data?.starships, "https://swapi.co/api/starships/"));
    });
  };

  useEffect(loadCharData, []);

  return !charData ? (
    <Loading />
  ) : (
    <Container>
      <CharacterDetails data={charData} id={id} shipsIds={starships} />
    </Container>
  );
};

export default Character;
